DEV_COMPOSE = dockerfiles/dev/docker-compose.yml
PROD_COMPOSE = dockerfiles/prod/docker-compose.yml

.PHONY: build
build:
	go build -o boats *.go

.PHONY: dev_env
dev_env: stop_dev_env
	docker-compose -f ${DEV_COMPOSE} up -d

.PHONY: stop_dev_env
stop_dev_env:
	docker-compose -f ${DEV_COMPOSE} down

.PHONY: prod_env
prod_env: stop_prod_env
	docker-compose -f ${PROD_COMPOSE} up -d

.PHONY: stop_prod_env
stop_prod_env:
	docker-compose -f ${PROD_COMPOSE} down

.PHONY:
tests:
	go test ./...
