# Boats searcher

### How to install
1) type `make prod_env` or `make dev_env` to install specific environment. Wait couple of seconds to make postgres settings up  
2) execute `make build` to compile **boats** application  
3) run `./boats -env=prod` or `./boats -env=dev`  
4) enjoy  

### API
Search:   */search?query=ab*  
Autocomplete */autocomplete?query=fe*  

### Incremental update
You can change some data in ./yachts_data/ files, then restart application to see what happened

### Tests
Use `make tests` to run tests
