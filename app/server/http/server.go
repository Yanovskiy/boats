package http

import (
	"bitbucket.org/Yanovskiy/boats/app/api/handlers"
	"bitbucket.org/Yanovskiy/boats/app/boats"
	"bitbucket.org/Yanovskiy/boats/logger"
	"github.com/buaazp/fasthttprouter"
	"github.com/jmoiron/sqlx"
	"github.com/spf13/viper"
	"github.com/valyala/fasthttp"
	"go.uber.org/zap"
)

const fastHttpReadBufferSize = 4096

func Start(db *sqlx.DB, datesCache boats.DatesCache) {
	router := fasthttprouter.New()

	// todo: add panic handler
	searchHandler := handlers.NewSearchHandler(boats.NewSearcher(db, datesCache))
	autoCompleteHandler := handlers.NewAutoCompleteHandler(boats.NewAutoCompleter(db))

	router.GET("/search", searchHandler)
	router.GET("/autocomplete", autoCompleteHandler)

	httpServer := &fasthttp.Server{
		Handler:        router.Handler,
		ReadBufferSize: fastHttpReadBufferSize,
	}
	addr := viper.GetString("http.address")

	go func() {
		err := httpServer.ListenAndServe(addr)
		if err != nil {
			logger.Get().Fatalw("error while starting http server", zap.Error(err))
		}

	}()
	logger.Get().Infow("http server started", "address", addr)
}
