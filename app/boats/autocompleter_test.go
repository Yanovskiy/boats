package boats

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAutoCompleter_AutoComplete(t *testing.T) {
	autoCompleter := NewAutoCompleter(db4tests)

	result, err := autoCompleter.AutoComplete(context.Background(), "mbe")
	assert.NoError(t, err)
	assert.Len(t, result, 1)
	assert.Exactly(t, result[0].YachtBuilderName, "builder_name_3")
	assert.Exactly(t, result[0].YachtModelName, "model_number_3")

	result, err = autoCompleter.AutoComplete(context.Background(), "del")
	assert.NoError(t, err)
	assert.Len(t, result, 3)
	assert.Exactly(t, result[0], AutoCompleteResponse{
		YachtBuilderName: "builder_name",
		YachtModelName:   "model_name",
	})
	assert.Exactly(t, result[1], AutoCompleteResponse{
		YachtBuilderName: "builder_name_3",
		YachtModelName:   "model_number_3",
	})
	assert.Exactly(t, result[2], AutoCompleteResponse{
		YachtBuilderName: "unique_builder_name",
		YachtModelName:   "unique_model_name",
	})

	result, err = autoCompleter.AutoComplete(context.Background(), "blabla")
	assert.NoError(t, err)
	assert.Len(t, result, 0)
}
