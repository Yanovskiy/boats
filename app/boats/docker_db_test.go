package boats

import (
	"fmt"
	"os"
	"testing"

	"bitbucket.org/Yanovskiy/boats/logger"
	"github.com/jmoiron/sqlx"
	"github.com/ory/dockertest"
)

var db4tests *sqlx.DB

func TestMain(m *testing.M) {
	pool, err := dockertest.NewPool("")
	if err != nil {
		logger.Get().Fatalf("Could not connect to docker: %s", err)
	}

	// pulls an image, creates a container based on it and runs it
	resource, err := pool.Run("postgres", "latest", []string{"POSTGRES_USER=developer"})
	if err != nil {
		logger.Get().Fatalf("Could not start resource: %s", err)
	}

	// exponential backoff-retry, because the application in the container might not be ready to accept connections yet
	if err := pool.Retry(func() error {
		var err error
		db4tests, err = sqlx.Open("postgres", fmt.Sprintf("postgres://developer:@localhost:%s/postgres?sslmode=disable", resource.GetPort("5432/tcp")))
		if err != nil {
			return err
		}

		return db4tests.Ping()
	}); err != nil {
		logger.Get().Fatalf("Could not connect to docker: %s", err)
	}

	applyMigrationsForTests()
	code := m.Run()

	// You can't defer this because os.Exit doesn't care for defer
	if err := pool.Purge(resource); err != nil {
		logger.Get().Fatalf("Could not purge resource: %s", err)
	}

	os.Exit(code)
}

func applyMigrationsForTests() {
	_, err := db4tests.Exec(createBoatsViewSql)
	if err != nil {
		logger.Get().Fatalf("could not connect create migrations: %s", err)
	}

	_, err = db4tests.Exec(insertBoatsSql)
	if err != nil {
		logger.Get().Fatalf("could not connect create migrations: %s", err)
	}

	// need for `levenshtein` function
	_, err = db4tests.Exec(`CREATE EXTENSION fuzzystrmatch;`)
	if err != nil {
		logger.Get().Fatalf("could not connect create migrations: %s", err)
	}
}

const (
	createBoatsViewSql = `
create table boats_view
(
	boat_id int not null
		constraint boats_v_pk
			primary key,
	yacht_model_name varchar not null,
	yacht_builder_name varchar not null,
	fleet_name varchar not null
)`

	insertBoatsSql = `INSERT INTO boats_view (boat_id, yacht_model_name, yacht_builder_name, fleet_name) VALUES
(1, 'model_name', 'builder_name', 'fleet_name'),
(2, 'unique_model_name', 'unique_builder_name', 'unique_fleet_name'),
(3, 'model_number_3', 'builder_name_3', 'fleet_name')`
)
