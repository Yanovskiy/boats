package boats

import "github.com/jackc/pgtype"

type DateRange struct {
	pgDateRange pgtype.Daterange
	From        string `json:"from"`
	To          string `json:"to"`
}

func (rd *DateRange) Scan(src interface{}) error {
	err := rd.pgDateRange.Scan(src)
	if err != nil {
		return err
	}

	rd.From = rd.pgDateRange.Lower.Time.Format("2006-01-02")
	rd.To = rd.pgDateRange.Upper.Time.Format("2006-01-02")

	return nil
}
