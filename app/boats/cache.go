package boats

import (
	"context"
	"sync"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
)

type DatesCache interface {
	Update(ctx context.Context) error
	UpcomingDatesAvailable(boatId uint32) (UpcomingDate, bool)
}

type UpcomingDateDb struct {
	BoatId            uint32     `db:"boat_id"`
	IsAvailableToday  bool       `db:"is_available_today"`
	UpcomingAvailable *DateRange `db:"upcoming_available_dates"`
}

func NewUpcomingAvailableDatesCache(db *sqlx.DB) *upcomingDatesAvailableCache {
	return &upcomingDatesAvailableCache{
		db:   db,
		m:    new(sync.RWMutex),
		data: new(map[uint32]UpcomingDate),
	}
}

type upcomingDatesAvailableCache struct {
	db *sqlx.DB

	m    *sync.RWMutex
	data *map[uint32]UpcomingDate
}

func (c *upcomingDatesAvailableCache) Update(ctx context.Context) error {
	var upcomingDates []UpcomingDateDb
	err := c.db.SelectContext(ctx, &upcomingDates, checkOnAvailabilitySql)
	if err != nil {
		return errors.Wrap(err, "error on check on availability sql")
	}

	c.set(upcomingDates)
	return nil
}

func (c *upcomingDatesAvailableCache) set(data []UpcomingDateDb) {
	newData := make(map[uint32]UpcomingDate, len(data))
	for _, item := range data {
		if itemFromData, ok := newData[item.BoatId]; !ok {
			newData[item.BoatId] = UpcomingDate{
				BoatId:           item.BoatId,
				IsAvailableToday: item.IsAvailableToday,
				UpcomingAvailable: []*DateRange{
					item.UpcomingAvailable,
				},
			}
		} else {
			itemFromData.UpcomingAvailable = append(itemFromData.UpcomingAvailable, item.UpcomingAvailable)
		}
	}

	c.m.Lock()
	*c.data = newData
	c.m.Unlock()
}

func (c *upcomingDatesAvailableCache) UpcomingDatesAvailable(boatId uint32) (UpcomingDate, bool) {
	c.m.RLock()
	defer c.m.RUnlock()

	m := *c.data
	val, ok := m[boatId]
	return val, ok
}

const (
	checkOnAvailabilitySql = `SELECT boat_id, upcoming_available_dates, (CASE
            WHEN upcoming_available_dates && daterange(current_date, current_date, '[]') THEN
                true
            ELSE
                false
           END) as is_available_today FROM upcoming_available_dates_view`
)

type nilDatesCache struct{}

func (n *nilDatesCache) Update(ctx context.Context) error { return nil }
func (n *nilDatesCache) UpcomingDatesAvailable(boatId uint32) (UpcomingDate, bool) {
	return UpcomingDate{}, true
}
