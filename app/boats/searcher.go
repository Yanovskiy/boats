package boats

import (
	"context"
	"strings"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
)

var (
	ErrEmptyQuery = errors.New("empty query on input")
)

type UpcomingDate struct {
	BoatId            uint32
	IsAvailableToday  bool
	UpcomingAvailable []*DateRange
}

type SearchResponse struct {
	BoatId            uint32       `json:"-" db:"boat_id"`
	YachtBuilderName  string       `json:"yacht_builder_name" db:"yacht_builder_name"`
	YachtModelName    string       `json:"yacht_model_name" db:"yacht_model_name"`
	FleetName         string       `json:"fleet_name" db:"fleet_name"`
	IsAvailableToday  bool         `json:"is_available_today" db:"is_available_today"`
	UpcomingAvailable []*DateRange `json:"upcoming_available_dates,omitempty" db:"upcoming_available_dates"`
}

type Searcher interface {
	Search(ctx context.Context, query string) ([]*SearchResponse, error)
}

type searcher struct {
	db         *sqlx.DB
	datesCache DatesCache
}

func NewSearcher(db *sqlx.DB, cache DatesCache) *searcher {
	return &searcher{
		db:         db,
		datesCache: cache,
	}
}

func (br *searcher) Search(ctx context.Context, query string) ([]*SearchResponse, error) {
	query = strings.TrimSpace(query)
	if query == "" {
		return nil, ErrEmptyQuery
	}

	var searchResponses []*SearchResponse
	query += "%"
	err := br.db.SelectContext(ctx, &searchResponses, searchBoatsSql, query)
	if err != nil {
		return nil, errors.Wrap(err, "error on search boats sql")
	}

	if len(searchResponses) == 0 {
		return []*SearchResponse{}, nil
	}

	br.setUpcomingAvailableDates(searchResponses)

	return searchResponses, nil
}

func (br *searcher) setUpcomingAvailableDates(searchResponses []*SearchResponse) {
	for i, boat := range searchResponses {
		dates, ok := br.datesCache.UpcomingDatesAvailable(boat.BoatId)
		if !ok {
			continue
		}

		searchResponses[i].IsAvailableToday = dates.IsAvailableToday
		searchResponses[i].UpcomingAvailable = dates.UpcomingAvailable
	}
}

const (
	searchBoatsSql = `
SELECT boat_id, yacht_builder_name, yacht_model_name, fleet_name
FROM boats_view b
WHERE lower(b.yacht_model_name) LIKE lower($1)
   OR lower(b.yacht_builder_name) LIKE lower($1)
`
)
