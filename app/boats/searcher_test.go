package boats

import (
	"context"
	"testing"

	_ "github.com/lib/pq"
	"github.com/stretchr/testify/assert"
)

func TestSearcher_Search(t *testing.T) {
	searcher := NewSearcher(db4tests, &nilDatesCache{})

	result, err := searcher.Search(context.Background(), "Uni")
	assert.NoError(t, err)
	assert.Len(t, result, 1)
	assert.Exactly(t, result[0].BoatId, uint32(2))

	result, err = searcher.Search(context.Background(), "Mod")
	assert.NoError(t, err)
	assert.NotNil(t, result)
	assert.Len(t, result, 2)
	assert.Exactly(t, result[0].BoatId, uint32(1))
	assert.Exactly(t, result[1].BoatId, uint32(3))

	result, err = searcher.Search(context.Background(), "blablabl")
	assert.NoError(t, err)
	assert.NotNil(t, result)
	assert.Len(t, result, 0)
}
