package boats

import (
	"context"

	"github.com/pkg/errors"

	"github.com/jmoiron/sqlx"
)

type AutoCompleteResponse struct {
	YachtBuilderName string `json:"yacht_builder_name" db:"yacht_builder_name"`
	YachtModelName   string `json:"yacht_model_name" db:"yacht_model_name"`
}

type AutoCompleter interface {
	AutoComplete(ctx context.Context, query string) ([]AutoCompleteResponse, error)
}

type autoCompleter struct {
	db *sqlx.DB
}

func NewAutoCompleter(db *sqlx.DB) *autoCompleter {
	return &autoCompleter{db: db}
}

func (ac *autoCompleter) AutoComplete(ctx context.Context, query string) ([]AutoCompleteResponse, error) {
	preparedQuery := "%" + query + "%"
	var autoCompleteResponses []AutoCompleteResponse
	err := ac.db.SelectContext(ctx, &autoCompleteResponses, autocompleteSql, preparedQuery, query)
	if err != nil {
		return nil, errors.Wrap(err, "autocomplete sql")
	}

	return autoCompleteResponses, nil
}

const (
	autocompleteSql = `SELECT yacht_builder_name, yacht_model_name
FROM boats_view b
WHERE lower(b.yacht_model_name) LIKE lower($1)
OR lower(b.yacht_builder_name) LIKE lower($1)
GROUP BY yacht_builder_name, yacht_model_name
ORDER BY levenshtein(yacht_model_name, $2);`
)
