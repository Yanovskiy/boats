package api

import (
	jsoniter "github.com/json-iterator/go"
	"github.com/valyala/fasthttp"
)

func JsonResponse(reqCtx *fasthttp.RequestCtx, model interface{}) error {
	jsonBody, err := jsoniter.Marshal(model)
	if err != nil {
		return err
	}

	reqCtx.SetContentType("application/json")
	reqCtx.SetStatusCode(fasthttp.StatusOK)
	_, err = reqCtx.Write(jsonBody)
	if err != nil {
		return err
	}

	return nil
}
