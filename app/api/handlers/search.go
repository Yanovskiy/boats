package handlers

import (
	"context"
	"strings"

	"bitbucket.org/Yanovskiy/boats/app/api"

	"bitbucket.org/Yanovskiy/boats/logger"
	"go.uber.org/zap"

	"bitbucket.org/Yanovskiy/boats/app/boats"
	"github.com/valyala/fasthttp"
)

func NewSearchHandler(boatsRepo boats.Searcher) func(*fasthttp.RequestCtx) {
	return func(reqCtx *fasthttp.RequestCtx) {
		query := strings.TrimSpace(string(reqCtx.QueryArgs().Peek("query")))
		if query == "" {
			reqCtx.SetStatusCode(fasthttp.StatusBadRequest)
			return
		}

		searchResponse, err := boatsRepo.Search(context.TODO(), query)
		if err != nil {
			logger.Get().Errorw("error search boats", zap.Error(err))
			reqCtx.SetStatusCode(fasthttp.StatusInternalServerError)
			return
		}

		err = api.JsonResponse(reqCtx, searchResponse)
		if err != nil {
			logger.Get().Errorw("error on search response", zap.Error(err))
			reqCtx.SetStatusCode(fasthttp.StatusInternalServerError)
			return
		}

		return
	}
}
