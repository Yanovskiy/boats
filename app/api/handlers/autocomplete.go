package handlers

import (
	"context"
	"strings"

	"bitbucket.org/Yanovskiy/boats/app/api"

	"bitbucket.org/Yanovskiy/boats/logger"
	"go.uber.org/zap"

	"bitbucket.org/Yanovskiy/boats/app/boats"
	"github.com/valyala/fasthttp"
)

func NewAutoCompleteHandler(autoCompleter boats.AutoCompleter) func(*fasthttp.RequestCtx) {
	return func(reqCtx *fasthttp.RequestCtx) {
		query := strings.TrimSpace(string(reqCtx.QueryArgs().Peek("query")))
		if query == "" {
			reqCtx.SetStatusCode(fasthttp.StatusBadRequest)
			return
		}

		hits, err := autoCompleter.AutoComplete(context.TODO(), query)
		if err != nil {
			logger.Get().Errorw("error on autocomplete", zap.Error(err))
			reqCtx.SetStatusCode(fasthttp.StatusInternalServerError)
			return
		}

		err = api.JsonResponse(reqCtx, hits)
		if err != nil {
			logger.Get().Errorw("error on hits response", zap.Error(err))
			reqCtx.SetStatusCode(fasthttp.StatusInternalServerError)
			return
		}

		return
	}
}
