module bitbucket.org/Yanovskiy/boats

go 1.13

require (
	github.com/Azure/go-ansiterm v0.0.0-20170929234023-d6e3b3328b78 // indirect
	github.com/DATA-DOG/go-sqlmock v1.3.3 // indirect
	github.com/Microsoft/go-winio v0.4.14 // indirect
	github.com/Nvveen/Gotty v0.0.0-20120604004816-cd527374f1e5 // indirect
	github.com/buaazp/fasthttprouter v0.1.1
	github.com/cenkalti/backoff v2.2.1+incompatible // indirect
	github.com/containerd/continuity v0.0.0-20190827140505-75bee3e2ccb6 // indirect
	github.com/docker/go-connections v0.4.0 // indirect
	github.com/docker/go-units v0.4.0 // indirect
	github.com/jackc/pgtype v1.0.2
	github.com/jackc/pgx/v4 v4.1.2
	github.com/jmoiron/sqlx v1.2.1-0.20190826204134-d7d95172beb5
	github.com/json-iterator/go v1.1.8
	github.com/klauspost/compress v1.9.1 // indirect
	github.com/lib/pq v1.2.0
	github.com/opencontainers/go-digest v1.0.0-rc1 // indirect
	github.com/opencontainers/image-spec v1.0.1 // indirect
	github.com/opencontainers/runc v0.1.1 // indirect
	github.com/ory/dockertest v3.3.5+incompatible
	github.com/pkg/errors v0.8.1
	github.com/spf13/viper v1.5.0
	github.com/stretchr/testify v1.4.0
	github.com/valyala/fasthttp v1.6.0
	go.uber.org/zap v1.12.0
)
