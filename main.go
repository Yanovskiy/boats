package main

import (
	"context"
	"flag"
	"os"
	"os/signal"
	"syscall"

	"bitbucket.org/Yanovskiy/boats/app/boats"
	"bitbucket.org/Yanovskiy/boats/app/server/http"
	"bitbucket.org/Yanovskiy/boats/logger"
	"bitbucket.org/Yanovskiy/boats/nausys"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/spf13/viper"
	"go.uber.org/zap"
)

var Env = flag.String("env", "dev", "environment (dev/prod)")

func init() {
	flag.Parse()
	logger.Init(*Env)
	initConfig()
}

func main() {
	ctx := ShutdownContext(context.Background())

	// set db connection
	db, err := sqlx.ConnectContext(ctx, viper.GetString("db.driver"), "user="+viper.GetString("db.user")+" dbname="+viper.GetString("db.name")+" sslmode=disable port="+viper.GetString("db.port"))
	if err != nil {
		logger.Get().Fatalf("fatal on postgres connect: %s \n", err)
	}
	defer func() {
		if err := db.Close(); err != nil {
			logger.Get().Errorw("unable to close db", zap.Error(err))
		}
	}()

	client := nausys.NewCsvClient(viper.GetString("nausys.pathToFiles"))

	datesCache := boats.NewUpcomingAvailableDatesCache(db)
	nausysDbLoader := nausys.NewDbLoader(db, client, datesCache)
	if err = nausysDbLoader.IncrementalUpdate(ctx); err != nil {
		logger.Get().Fatalf("error on nausys db loader", zap.Error(err))
	}

	http.Start(db, datesCache)

	<-ctx.Done()
	logger.Get().Infow("app successfully exit")
}

func initConfig() {
	viper.AddConfigPath("config")
	viper.SetConfigType("yml")
	viper.SetConfigName(*Env)
	if err := viper.ReadInConfig(); err != nil {
		logger.Get().Panicf("fatal on read config file: %s \n", err)
	}
}

// ShutdownContext returns child context from passed context which will be canceled
// on incoming signals: SIGINT, SIGTERM, SIGHUP
func ShutdownContext(c context.Context) context.Context {
	ctx, cancel := context.WithCancel(c)
	go func() {
		ch := make(chan os.Signal, 1)
		signal.Notify(ch, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
		select {
		case <-ctx.Done():
			return
		case <-ch:
			cancel()
		}
	}()
	return ctx
}
