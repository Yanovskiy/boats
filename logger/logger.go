package logger

import (
	"log"

	"go.uber.org/zap"
)

var logger *zap.SugaredLogger

func Init(env string) {
	var zapLogger *zap.Logger
	var err error

	if env == "prod" {
		zapLogger, err = zap.NewProduction()
	} else {
		zapLogger, err = zap.NewDevelopment()
	}

	if err != nil {
		log.Fatalf("unable to init zap logger: %s", err.Error())
	}

	logger = zapLogger.Sugar()
}

func Get() *zap.SugaredLogger {
	return logger
}
