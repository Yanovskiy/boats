create table yacht_builders
(
    id integer not null
        constraint yacht_builders_pk
            primary key,
    name varchar(255) not null
);

alter table yacht_builders owner to developer;

create table yacht_models
(
    id integer not null
        constraint yacht_models_pk
            primary key,
    name varchar(255),
    yacht_builder_id integer not null
        constraint yacht_models_yacht_builders_id_fk
            references yacht_builders
);

alter table yacht_models owner to developer;


create table fleets
(
    id integer not null
        constraint fleets_pk
            primary key,
    name varchar(255) not null
);

alter table fleets owner to developer;


create table boats
(
    id integer not null
        constraint boats_pk
            primary key,
    fleet_id integer not null
        constraint boats_fleets_id_fk
            references fleets,
    model_id integer
        constraint boats_yacht_models_id_fk
            references yacht_models
);

alter table boats owner to developer;

create table calendars
(
    boat_id integer not null
        constraint calendars_boats_id_fk
            references boats,
    from_to daterange not null,
    available boolean not null
);

alter table calendars owner to developer;

create unique index calendars_boat_id_from_to_uindex
    on calendars (boat_id, from_to);

-- need for levenshtein function
CREATE EXTENSION fuzzystrmatch;

CREATE MATERIALIZED VIEW boats_view AS
SELECT b.id     as boat_id,
       m.name   as yacht_model_name,
       yb.name  as yacht_builder_name,
       f.name   as fleet_name
FROM yacht_models m
         JOIN yacht_builders yb on m.yacht_builder_id = yb.id
         JOIN boats b on m.id = b.model_id
         JOIN fleets f on b.fleet_id = f.id
GROUP BY b.id, m.name, yb.name, f.name
    WITH NO DATA;
create index yacht_model_name_index
    on boats_view (lower(yacht_model_name) varchar_pattern_ops);
create index yacht_builder_name_index
    on boats_view (lower(yacht_builder_name) varchar_pattern_ops);
REFRESH MATERIALIZED VIEW boats_view;

CREATE MATERIALIZED VIEW upcoming_available_dates_view AS
SELECT b.id as   boat_id,
       c.from_to upcoming_available_dates
FROM boats b
         LEFT JOIN calendars c on b.id = c.boat_id
WHERE c.from_to &&
      daterange(current_date, (current_date + interval '30 days')::date, '[]')
    WITH NO DATA;
create index upcoming_available_dates_id_index on upcoming_available_dates_view (boat_id);
REFRESH MATERIALIZED VIEW upcoming_available_dates_view;

