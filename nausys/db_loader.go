package nausys

import (
	"context"

	"go.uber.org/zap"

	"bitbucket.org/Yanovskiy/boats/app/boats"

	"bitbucket.org/Yanovskiy/boats/nausys/models"

	"bitbucket.org/Yanovskiy/boats/logger"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
)

const maxBatchCount = 1000

func NewDbLoader(db *sqlx.DB, client Client, datesCache boats.DatesCache) *dbLoader {
	return &dbLoader{
		db:         db,
		client:     client,
		datesCache: datesCache,
	}
}

type dbLoader struct {
	db         *sqlx.DB
	client     Client
	datesCache boats.DatesCache
}

func (l *dbLoader) IncrementalUpdate(ctx context.Context) (err error) {
	fleets, err := l.client.Fleets(ctx)
	if err != nil {
		return
	}

	builders, err := l.client.YachtBuilders(ctx)
	if err != nil {
		return
	}

	yachtModels, err := l.client.YachtModels(ctx)
	if err != nil {
		return
	}

	boatsData, err := l.client.Boats(ctx)
	if err != nil {
		return
	}

	calendars, err := l.client.Calendars(ctx)
	if err != nil {
		return
	}

	tx, err := l.db.Beginx()
	if err != nil {
		return
	}

	defer func() {
		if err != nil {
			if rbErr := tx.Rollback(); rbErr != nil {
				logger.Get().Errorw("db loader rollback error", zap.Error(rbErr))
			}
		}
	}()

	logger.Get().Infow("start update db")
	_, err = tx.NamedExecContext(ctx, `INSERT INTO fleets (id, name) VALUES (:id, :name)
ON CONFLICT (id) DO UPDATE SET name = EXCLUDED.name`, fleets)
	if err != nil {
		return errors.Wrap(err, "error on fleets insert")
	}

	_, err = tx.NamedExecContext(ctx, `INSERT INTO yacht_builders (id, name) VALUES (:id, :name)
ON CONFLICT (id) DO UPDATE SET name = EXCLUDED.name`, builders)
	if err != nil {
		return errors.Wrap(err, "error on yacht_builders insert")
	}

	_, err = tx.NamedExecContext(ctx, `INSERT INTO yacht_models (id, name, yacht_builder_id)
VALUES (:id, :name, :yacht_builder_id) ON CONFLICT (id) DO UPDATE
SET name = EXCLUDED.name, yacht_builder_id = EXCLUDED.yacht_builder_id`, yachtModels)
	if err != nil {
		return errors.Wrap(err, "error on yacht_models insert")
	}

	_, err = tx.NamedExecContext(ctx, `INSERT INTO boats (id, fleet_id, model_id) VALUES 
		(:id, :fleet_id, :model_id) ON CONFLICT (id) DO UPDATE
SET fleet_id = EXCLUDED.fleet_id, model_id = EXCLUDED.model_id`, boatsData)
	if err != nil {
		return errors.Wrap(err, "error on boats insert")
	}

	err = l.insertCalendarsByChunks(ctx, tx, calendars)
	if err != nil {
		return errors.Wrap(err, "error on calendars insert")
	}

	_, err = tx.ExecContext(ctx, `REFRESH MATERIALIZED VIEW boats_view;`)
	if err != nil {
		return errors.Wrap(err, "error on refresh boats_view")
	}

	_, err = tx.ExecContext(ctx, `REFRESH MATERIALIZED VIEW upcoming_available_dates_view;`)
	if err != nil {
		return errors.Wrap(err, "error on refresh boats_view")
	}

	if err := tx.Commit(); err != nil {
		logger.Get().Errorw("db loader commit error", zap.Error(err))
	}

	err = l.datesCache.Update(ctx)
	if err != nil {
		return errors.Wrap(err, "error on update dates cache")
	}

	logger.Get().Infow("finish update db")
	return nil
}

// prepare for bulk insert 'cause PostgreSQL have limit on placeholders count (65535)
func (l *dbLoader) insertCalendarsByChunks(ctx context.Context, tx *sqlx.Tx, calendars []models.Calendar) error {
	var batches [][]models.Calendar
	for maxBatchCount < len(calendars) {
		calendars, batches = calendars[maxBatchCount:], append(batches, calendars[0:maxBatchCount:maxBatchCount])
	}
	batches = append(batches, calendars)
	for _, batch := range batches {
		_, err := tx.NamedExecContext(ctx, `INSERT INTO calendars (boat_id, from_to, available)
VALUES (:boat_id, :from_to, :available) ON CONFLICT (boat_id, from_to) DO UPDATE
SET available = EXCLUDED.available`, batch)
		if err != nil {
			return err
		}
	}

	return nil
}
