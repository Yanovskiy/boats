package nausys

import (
	"bufio"
	"context"
	"database/sql"
	"encoding/csv"
	"io"
	"os"
	"strconv"

	"bitbucket.org/Yanovskiy/boats/logger"
	"bitbucket.org/Yanovskiy/boats/nausys/models"
	"github.com/jackc/pgtype"
	"go.uber.org/zap"
)

func NewCsvClient(pathToFiles string) *csvClient {
	return &csvClient{
		pathToFiles: pathToFiles,
	}
}

type csvClient struct {
	pathToFiles string
}

func (f *csvClient) Boats(ctx context.Context) ([]models.Boat, error) {
	csvFile, err := os.Open(f.pathToFiles + "boats.csv")
	if err != nil {
		return nil, err
	}
	defer func() {
		if err := csvFile.Close(); err != nil {
			logger.Get().Errorw("unable to close file", zap.Error(err))
		}
	}()

	reader := csv.NewReader(bufio.NewReader(csvFile))
	_, err = reader.Read() // skip headers
	if err != nil {
		return nil, err
	}

	var boats []models.Boat
	for {
		select {
		case <-ctx.Done():
			return nil, ctx.Err()
		default:
		}
		line, err := reader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			return nil, err
		}

		id, err := strconv.Atoi(line[0])
		if err != nil {
			return nil, err
		}

		fleetId, err := strconv.Atoi(line[1])
		if err != nil {
			return nil, err
		}

		modelId := sql.NullInt32{}
		if line[2] != "" {
			mId, err := strconv.Atoi(line[2])
			if err != nil {
				return nil, err
			}

			modelId = sql.NullInt32{
				Int32: int32(mId),
				Valid: true,
			}
		}

		boats = append(boats, models.Boat{
			Id:      id,
			FleetId: fleetId,
			ModelId: modelId,
		})
	}

	return boats, nil
}

func (f *csvClient) Calendars(ctx context.Context) ([]models.Calendar, error) {
	csvFile, err := os.Open(f.pathToFiles + "calendar.csv")
	if err != nil {
		return nil, err
	}
	defer func() {
		if err := csvFile.Close(); err != nil {
			logger.Get().Errorw("unable to close file", zap.Error(err))
		}
	}()

	reader := csv.NewReader(bufio.NewReader(csvFile))
	_, err = reader.Read() // skip headers
	if err != nil {
		return nil, err
	}

	var calendars []models.Calendar
	for {
		select {
		case <-ctx.Done():
			return nil, ctx.Err()
		default:
		}
		line, err := reader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			return nil, err
		}

		boatId, err := strconv.Atoi(line[0])
		if err != nil {
			return nil, err
		}

		available, err := strconv.ParseBool(line[3])

		// inclusive — “[”
		// exclusive — “)”
		fromTo := &pgtype.Daterange{}
		err = fromTo.DecodeText(nil, []byte("["+line[1]+","+line[2]+")"))
		if err != nil {
			return nil, err
		}

		calendar := models.Calendar{
			BoatId:    boatId,
			FromTo:    *fromTo,
			Available: available,
		}

		calendars = append(calendars, calendar)
	}

	return f.deduplicateCalendars(calendars), nil
}

// Need to deduplicate items only for "bulk insert mode", otherwise pq library throw an error
// pq: ON CONFLICT DO UPDATE command cannot affect row a second time
// in "single mode insert" deduplication is not needed (no errors from pq), but performance is very low
func (f *csvClient) deduplicateCalendars(calendars []models.Calendar) []models.Calendar {
	calendarsMap := make(map[models.Calendar]struct{})
	out := calendars[:0]
	i := 0
	for _, calendar := range calendars {
		if _, ok := calendarsMap[calendar]; ok {
			i++
			continue
		}

		calendarsMap[calendar] = struct{}{}
		out = append(out, calendar)
	}

	if i > 0 {
		logger.Get().Debugf("detected %d duplicated items in calendar, filtered", i)
	}

	return out
}

func (f *csvClient) Fleets(ctx context.Context) ([]models.Fleet, error) {
	csvFile, err := os.Open(f.pathToFiles + "fleets.csv")
	if err != nil {
		return nil, err
	}
	defer func() {
		if err := csvFile.Close(); err != nil {
			logger.Get().Errorw("unable to close file", zap.Error(err))
		}
	}()

	reader := csv.NewReader(bufio.NewReader(csvFile))
	_, err = reader.Read() // skip headers
	if err != nil {
		return nil, err
	}

	var fleets []models.Fleet
	for {
		select {
		case <-ctx.Done():
			return nil, ctx.Err()
		default:
		}
		line, err := reader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			return nil, err
		}

		id, err := strconv.Atoi(line[0])
		if err != nil {
			return nil, err
		}

		fleets = append(fleets, models.Fleet{
			Id:   id,
			Name: line[1],
		})
	}

	return fleets, nil
}

func (f *csvClient) YachtModels(ctx context.Context) ([]models.YachtModel, error) {
	csvFile, err := os.Open(f.pathToFiles + "yacht_models.csv")
	if err != nil {
		return nil, err
	}
	defer func() {
		if err := csvFile.Close(); err != nil {
			logger.Get().Errorw("unable to close file", zap.Error(err))
		}
	}()

	reader := csv.NewReader(bufio.NewReader(csvFile))
	_, err = reader.Read() // skip headers
	if err != nil {
		return nil, err
	}

	var yachtModels []models.YachtModel
	for {
		select {
		case <-ctx.Done():
			return nil, ctx.Err()
		default:
		}
		line, err := reader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			return nil, err
		}

		id, err := strconv.Atoi(line[0])
		if err != nil {
			return nil, err
		}

		builderId, err := strconv.Atoi(line[2])
		if err != nil {
			return nil, err
		}

		yachtModels = append(yachtModels, models.YachtModel{
			Id:             id,
			Name:           line[1],
			YachtBuilderId: builderId,
		})
	}

	return yachtModels, nil
}

func (f *csvClient) YachtBuilders(ctx context.Context) ([]models.YachtBuilder, error) {
	csvFile, err := os.Open(f.pathToFiles + "yacht_builders.csv")
	if err != nil {
		return nil, err
	}
	defer func() {
		if err := csvFile.Close(); err != nil {
			logger.Get().Errorw("unable to close file", zap.Error(err))
		}
	}()

	reader := csv.NewReader(bufio.NewReader(csvFile))
	_, err = reader.Read() // skip headers
	if err != nil {
		return nil, err
	}

	var builders []models.YachtBuilder
	for {
		select {
		case <-ctx.Done():
			return nil, ctx.Err()
		default:
		}

		line, err := reader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			return nil, err
		}

		id, err := strconv.Atoi(line[0])
		if err != nil {
			return nil, err
		}

		builders = append(builders, models.YachtBuilder{
			Id:   id,
			Name: line[1],
		})
	}

	return builders, nil
}
