package nausys

import (
	"context"

	"bitbucket.org/Yanovskiy/boats/nausys/models"
)

type Client interface {
	Boats(context.Context) ([]models.Boat, error)
	Calendars(context.Context) ([]models.Calendar, error)
	Fleets(context.Context) ([]models.Fleet, error)
	YachtModels(context.Context) ([]models.YachtModel, error)
	YachtBuilders(context.Context) ([]models.YachtBuilder, error)
}
