package models

import "database/sql"

type Boat struct {
	Id      int
	FleetId int           `db:"fleet_id"`
	ModelId sql.NullInt32 `db:"model_id"`
}
