package models

import "github.com/jackc/pgtype"

type Calendar struct {
	BoatId    int              `db:"boat_id"`
	FromTo    pgtype.Daterange `db:"from_to"`
	Available bool             `db:"available"`
}
