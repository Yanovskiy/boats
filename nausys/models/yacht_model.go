package models

type YachtModel struct {
	Id             int
	Name           string
	YachtBuilderId int `db:"yacht_builder_id"`
}
